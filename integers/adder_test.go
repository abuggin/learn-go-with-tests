package integers

import (
	"fmt"
	"testing"
)

func TestAdder(t *testing.T) {
	sum := Add(40, 2)
	expected := 42

	if sum != expected {
		t.Errorf("got '%d', expected '%d'", sum, expected)
	}
}

func ExampleAdd() {
	sum := Add(10, 5)
	fmt.Print(sum)
	// Output: 15
}
