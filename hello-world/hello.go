package main

import "fmt"

func getSalutation(locale string) string {
	switch locale {
	case "spa":
		return "Hola, "
	case "fra":
		return "Bonjour, "
	default:
		return "Hello, "
	}
}

// Hello ...
func Hello(name, locale string) string {

	prefix := getSalutation(locale)
	if name == "" {
		return prefix + "world"
	}
	return prefix + name
}

func main() {
	fmt.Println(Hello("", ""))
}
