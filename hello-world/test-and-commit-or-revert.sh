#!/bin/bash
set -e 
go test -v && git add --all . && git commit -v || ls | awk '$0 !~ /test/' | xargs git checkout HEAD --