package main

import "testing"

func TestHelloWorld(t *testing.T) {
	assertCorrectMessage := func(t *testing.T, got, want string) {
		t.Helper()
		if got != want {
			t.Errorf("got '%s' wants '%s'", got, want)
		}
	}
	t.Run("saying hello to people", func(t *testing.T) {

		got := Hello("Johnny", "")
		want := "Hello, Johnny"

		assertCorrectMessage(t, got, want)
	})
	t.Run("say 'Hello, World' when an empty string is supplied", func(t *testing.T) {

		got := Hello("", "")
		want := "Hello, world"

		assertCorrectMessage(t, got, want)
	})
	t.Run("saying hello in Spanish", func(t *testing.T) {

		got := Hello("Johnny", "spa")
		want := "Hola, Johnny"

		assertCorrectMessage(t, got, want)
	})
	t.Run("saying hello in French", func(t *testing.T) {

		got := Hello("Johnny", "fra")
		want := "Bonjour, Johnny"

		assertCorrectMessage(t, got, want)
	})
}
