package slices

// Sum ...
func Sum(numbers []int) int {
	sum := 0
	for _, n := range numbers {
		sum += n
	}
	return sum
}

// SumAll ...
func SumAll(numbersToSum ...[]int) (sums []int) {
	for _, sum := range numbersToSum {
		sums = append(sums, Sum(sum))
	}
	return sums
}

// SumAllTails ...
func SumAllTails(numbersToSum ...[]int) (sums []int) {
	for _, sum := range numbersToSum {
		if len(sum) > 0 {
			sums = append(sums, Sum(sum[1:]))
		} else {
			sums = append(sums, 0)
		}
	}
	return sums
}
