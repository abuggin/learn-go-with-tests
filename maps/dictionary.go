package dictionary

import "errors"

// Dictionary ...
type Dictionary map[string]string

// ErrorNotFound ...
var ErrorNotFound = errors.New("could not find the word you were looking for")

// ErrorAlreadyDefined ...
var ErrorAlreadyDefined = errors.New("could not add word because key already present")

// Search in a Dictionary just return the value in its underlying map
func (d Dictionary) Search(word string) (string, error) {
	definition, ok := d[word]
	if !ok {
		return "", ErrorNotFound
	}
	return definition, nil
}

// Add a new key value pair to the dictionary
func (d Dictionary) Add(key, value string) error {
	valueFound, err := d.Search(key)
	if valueFound == "" && err == ErrorNotFound {
		d[key] = value
	}
	return ErrorAlreadyDefined
}
