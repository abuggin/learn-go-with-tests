package dictionary

import "testing"

func TestSearch(t *testing.T) {
	t.Run("Dictionary provides existing value for existing key", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		got, _ := dictionary.Search("test")
		want := "this is just a test"

		assertStrings(t, got, want)
	})
	t.Run("Dictionary errors out when can't find key", func(t *testing.T) {
		dictionary := Dictionary{"test": "this is just a test"}
		_, err := dictionary.Search("no")
		if err == nil {
			t.Fatal("expected to get an error.")
		}
		assertErrors(t, err, ErrorNotFound)
	})

	t.Run("Dictionary provides a way to add new key-values", func(t *testing.T) {
		dict := Dictionary{}
		want := "value"
		dict.Add("key", want)
		got, ok := dict.Search("key")
		if ok != nil {
			t.Fatal("should find added word:", ok)
		}

		if want != got {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	})
	t.Run("Dictionary preserve values", func(t *testing.T) {
		key := "key"
		want := "value"
		dict := Dictionary{key: want}
		err := dict.Add(key, "some different value")
		got, _ := dict.Search(key)
		assertErrors(t, err, ErrorAlreadyDefined)
		if want != got {
			t.Errorf("got '%s' want '%s'", got, want)
		}
	})

}
func assertErrors(t *testing.T, got, want error) {
	t.Helper()

	if got != want {
		t.Errorf("got: '%v', want: '%v'", got, want)
	}
}

func assertStrings(t *testing.T, got, want string) {
	t.Helper()

	if got != want {
		t.Errorf("got: '%v', want: '%v'", got, want)
	}
}
