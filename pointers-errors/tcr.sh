#!/bin/bash
set -e 
go test -cover -v && git add --all . && git commit -v || ls | awk '$0 !~ /test/' | xargs git checkout HEAD --