package errors

import (
	"testing"
)

func TestWallet(t *testing.T) {

	assertNoError := func(t *testing.T, got error) {
		t.Helper()
		if got != nil {
			t.Fatalf("not expecting error, got %v", got)
		}
	}

	assertError := func(t *testing.T, got error, want error) {
		t.Helper()
		if got == nil {
			t.Fatal("not got an error, expecting one")
		}
		if want != got {
			t.Fatalf("got %s want %s", got, want)
		}
	}
	assertBalance := func(t *testing.T, wallet Wallet, want Bitcoin) {
		t.Helper()
		got := wallet.Balance()

		if got != want {
			t.Errorf("got %s want %s", got, want)
		}
	}

	t.Run("Stringer", func(t *testing.T) {
		want := Bitcoin(10)
		if want.String() != "10 BTC" {
			t.Errorf("want %s", want)
		}
	})

	t.Run("Deposit", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))
		want := Bitcoin(10)

		assertBalance(t, wallet, want)
	})
	t.Run("Withdraw", func(t *testing.T) {
		wallet := Wallet{}
		wallet.Deposit(Bitcoin(10))
		err := wallet.Withdraw(Bitcoin(5))
		want := Bitcoin(5)

		assertBalance(t, wallet, want)
		assertNoError(t, err)
	})

	t.Run("Withdraw insufficient funds", func(t *testing.T) {
		startingBalance := Bitcoin(20)
		wallet := Wallet{startingBalance}
		err := wallet.Withdraw(Bitcoin(100))

		assertBalance(t, wallet, startingBalance)
		assertError(t, err, ErrInsufficientFunds)
		if err == nil {
			t.Error("wanted an error but didn't get one")
		}
	})

}
