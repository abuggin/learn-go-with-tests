# hello-go

[Following this amazing book _Learn Go with tests_](https://quii.gitbook.io/learn-go-with-tests/go-fundamentals/hello-world)

Implemented Hello World using [TC||R](https://medium.com/@kentbeck_7670/test-commit-revert-870bbd756864)

> The full command then is `test && commit || revert`. If the tests fail, then the code goes back to the state where the tests last passed.
