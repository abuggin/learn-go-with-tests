package iteration

import "strings"

// Repeat ...
func Repeat(str string, times int) string {
	return strings.Repeat(str, times)
}
