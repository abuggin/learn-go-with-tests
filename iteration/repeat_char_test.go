package iteration

import "testing"

func TestRepeater(t *testing.T) {

	expected := "ccccc"
	actual := Repeat("c", 5)

	if expected != actual {
		t.Errorf("")
	}
}

func BenchmarkRepeat(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Repeat("a", 8)
	}
}
