package interfaces

import (
	"math"
)

// Perimeter ...
func Perimeter(rectangle Rectangle) float64 {
	return (rectangle.Height + rectangle.Width) * 2
}

// Rectangle ...
type Rectangle struct {
	Width  float64
	Height float64
}

// Circle ...
type Circle struct {
	Radius float64
}

// Triangle ...
type Triangle struct {
	Base   float64
	Height float64
}

// Area for a Triangle
func (t Triangle) Area() float64 {
	return t.Height * t.Base / 2
}

// Area for a Rectangle
func (r Rectangle) Area() float64 {
	return r.Height * r.Width
}

// Area for a Circle
func (c Circle) Area() float64 {
	return math.Pi * c.Radius * c.Radius
}

// Shape is anything that implements Area
type Shape interface {
	Area() float64
}
